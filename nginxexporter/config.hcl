listen {
  port = 4040
  address = "0.0.0.0"
}

namespace "cms_backend" {
  format = "$remote_addr - $remote_user [$time_local] \"$request\" $status $body_bytes_sent \"$http_referer\" \"$http_user_agent\" $upstream_response_time"
  source_files = ["/var/log/nginx/access.log"]

  relabel "request_uri" {
    from = "request"
  }
}
